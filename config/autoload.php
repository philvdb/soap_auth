<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2016 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'SOAPAuth',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'SOAPAuth\Server' => 'system/modules/soap_auth/classes/Server.php',
	'SOAPAuth\Tags'   => 'system/modules/soap_auth/classes/Tags.php',
));
