<?php


namespace SOAPAuth;


class Tags extends \System
{
    public function replaceInsertTags($strTag, $blnCache, $strCache, $flags, $tags, $arrCache, $_rit, $_cnt)
    {
        if (strtok($strTag, '::') !== 'soap_auth') return false;

        $arrElements = explode('::', $strTag);

        // We expect an external link target as first argument of the insert tag
        if (strpos($arrElements[1], 'http') === false)
        {
            $this->log('Das InsertTag soap_auth erwartet als erstes Argument einen validen Link zum Zielserver.', __METHOD__, TL_ERROR);
            return 'FEHLER (siehe Contao-Log).';
        }

        // User must be logged in for this to work
        if (!FE_USER_LOGGED_IN)
        {
            return 'Der Link wird hier nach Login angezeigt.';
        }

        // We can now create a unique password and save it with the user's data
        $this->import('FrontendUser', 'User');

        $this->User->soapPass = md5(uniqid(mt_rand(), true));
        $this->User->save();

        $strArgDelimiter = strpos($arrElements[1], '?') === false ? '?' : '&';

        return sprintf('<a href="%s%sext_uid=%s&soap_pw=%s">%s</a>',
            $arrElements[1],
            $strArgDelimiter,
            $this->User->id,
            $this->User->soapPass,
            $arrElements[2] ?: 'Hier klicken'
            );
    }
}