<?php


namespace SOAPAuth;


class Server
{
    function isValidSession($intUserId = null, $strSoapPw = null)
    {
        if ($intUserId && $strSoapPw)
        {
            $objUser = \MemberModel::findOneBy(['id=?', 'soapPass=?'], [$intUserId, $strSoapPw]);

            if (is_object($objUser))
            {
                return [
                    'valid' => true,
                    'firstname' => \StringUtil::decodeEntities($objUser->firstname),
                    'lastname' => \StringUtil::decodeEntities($objUser->lastname),
                    'gender' => $objUser->gender == 'female' ? 'f' : 'm', // Seems ILIAS will not import this
                    'email' => $objUser->email
                ];
            }
        }

        return [
            'valid' => false
        ];
    }
}
